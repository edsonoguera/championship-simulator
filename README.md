*Objetivo: Criar um simulador de partidas, onde, através de uma tabela principal, serão apresentados os vencedores da rodada atual.

1 - A cada rodada, o vencedor receberá 3 pontos, o time derrotado não somará pontos, e em caso de empate, 1 ponto para ambos.

2 - Os times deverão se enfrentar de forma aleatória a cada rodada, respeitando os seguintes critérios:

    2.a) Os times não podem se enfrentar mais de duas vezes durante a competição;
    2.b) O mesmo time não pode se enfrentar;
    

Guia de desenvolvimento (poderá ser alterado no decorrer do desenvolvimento): 

1 - Faça uma classe pai para definir os atributos de um time da liga.

2 - Faça com que os outros times sejam instancias da classe pai

3 - Cada classe de time deverá ter um array, e esse servirá para guardar os seus resultados de vitória.

    3.a - Estes resultados serão somados, para decretar o campeão da competição ao final de todas as rodadas.

=========================== CARACTERISTICAS DE UM TIME ===========================
A - NOME DA EQUIPE;
B - COR DO UNIFORME;
C - ARRAY DE PONTOS (Método da classe)