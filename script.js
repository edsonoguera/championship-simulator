class Team {
    constructor(equipe, uniforme){
        this.equipe = equipe;
        this.uniforme = uniforme;
    }

    pontos = [];

    resultado (condicao) {
        if (this.pontos.length < 5){
            this.pontos.push(condicao)
            for (let i = 0; i < this.pontos.length; i++){
                //conversão do resultado da partida em pontos
                if (this.pontos[i] === 'vitoria'){
                    this.pontos[i] = 3
                }else if (this.pontos[i] === 'empate'){
                    this.pontos[i] = 1
                }else if (this.pontos[i] === 'derrota'){
                    this.pontos[i] = 0
                }
            }
        }                              
    }

    amountOfPoints () {
        let sum = 0;
        for (let i = 0; i < this.pontos.length; i++){
            sum += this.pontos[i];
        }
        return sum;
    }
}


const westBromwich = new Team('West Bromwich','Azul')
console.log(westBromwich)

const leedsUtd = new Team('Leeds United', 'Branco')
console.log(leedsUtd)



westBromwich.resultado('vitoria')
westBromwich.resultado('empate')
westBromwich.resultado('vitoria')
westBromwich.resultado('derrota')
westBromwich.resultado('empate')
westBromwich.resultado('empate') //Não aceita mais resultados.
console.log(`O resultado da partida foi: ${westBromwich.resultado()}`)
console.log(`Agora, o ${westBromwich.equipe} está com ${westBromwich.amountOfPoints()} pontos.`)
console.log(westBromwich.pontos)